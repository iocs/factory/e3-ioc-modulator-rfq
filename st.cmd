require essioc
require modbus
require modulator

epicsEnvSet(SYS, "RFQ")
epicsEnvSet(SUB, "010")
epicsEnvSet(ID, "001")
epicsEnvSet(MOD_IP, "rfq-010-modulator.tn.esss.lu.se")
epicsEnvSet(PSS_PV, "FEB-010Row:CnPw-U-004:ToRFQLPS")

iocshLoad("$(essioc_DIR)/common_config.iocsh")

iocshLoad("$(modulator_DIR)/modulator.iocsh", "SYS=$(SYS), SUB=$(SUB), ID=$(ID), MOD_IP=$(MOD_IP)")

dbLoadRecords("$(E3_CMD_TOP)/pss_bypass.db","P=$(SYS)-$(SUB):RFS-Mod-$(ID):")

pvlistFromInfo("ARCHIVE_THIS","$(IOCNAME):ArchiverList")

